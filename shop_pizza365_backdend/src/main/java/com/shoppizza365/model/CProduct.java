package com.shoppizza365.model;

import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="products")
public class CProduct {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="product_code", unique = true)
	private String productCode;
	
	@Column(name="product_name")
	private String productName;
	
	@Column(name="product_description")
	private String productDescription;
	
	@ManyToOne
	@JsonIgnore
	private CProductLine product_line;
	
	@Transient
	private long productLineId;
	
	@Column(name="product_scale")
	private String productScale;
	
	@Column(name="product_vendor")
	private String productVendor;
	
	@Column(name="quantity_in_stock")
	private Long quantityInStock;
	
	@Column(name="buy_price")
	private float buyPrice;

	@OneToMany(targetEntity = COrderDetail.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "product_id")
	private List<COrderDetail> orderDetail;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	@JsonIgnore
	public CProductLine getProductLine() {
		return product_line;
	}

	public void setProductLine(CProductLine product_line) {
		this.product_line = product_line;
	}

	public String getProductScale() {
		return productScale;
	}

	public void setProductScale(String productScale) {
		this.productScale = productScale;
	}

	public String getProductVendor() {
		return productVendor;
	}

	public void setProductVendor(String productVendor) {
		this.productVendor = productVendor;
	}

	public Long getQuantityInStock() {
		return quantityInStock;
	}

	public void setQuantityInStock(Long quantityInStock) {
		this.quantityInStock = quantityInStock;
	}

	public float getBuyPrice() {
		return buyPrice;
	}

	public void setBuyPrice(float buyPrice) {
		this.buyPrice = buyPrice;
	}

	@JsonIgnore
	public List<COrderDetail> getOrderDetail() {
		return orderDetail;
	}

	public void setOrderDetail(List<COrderDetail> orderDetail) {
		this.orderDetail = orderDetail;
	}

	public long getProductLineId() {
		return product_line.getId();
	}

	public void setProductLineId(long productLineId) {
		this.productLineId = productLineId;
	}
}
