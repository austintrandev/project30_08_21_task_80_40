package com.shoppizza365.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.shoppizza365.model.*;

public interface IEmployeeRepository extends JpaRepository<CEmployee, Long> {
	@Query(value = "SELECT * FROM employees  WHERE first_name like %:name% OR last_name like %:name%", nativeQuery = true)
	List<CEmployee> findEmployeeByFirstOrLastName(@Param("name") String name);
	
	@Query(value = "SELECT * FROM employees  WHERE first_name like %:name% OR last_name like %:name%", nativeQuery = true)
	List<CEmployee> findEmployeeByFirstOrLastNamePageable(@Param("name") String name,Pageable pageable);
	
	@Query(value = "SELECT * FROM employees  WHERE first_name like %:name% OR last_name like %:name% ORDER BY first_name DESC", nativeQuery = true)
	List<CEmployee> findEmployeeByFirstOrLastNameDESC(@Param("name") String name,Pageable pageable);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE employees SET job_title = :jobtitle WHERE job_title IS NULL", nativeQuery = true)
	int updateJobTitle(@Param("jobtitle") String jobtitle);
}
