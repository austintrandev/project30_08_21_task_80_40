package com.shoppizza365.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.shoppizza365.model.CProduct;

public interface IProductRepository extends JpaRepository<CProduct, Long> {
	@Query(value = "SELECT * FROM products  WHERE product_name like %:name%", nativeQuery = true)
	List<CProduct> findProductByName(@Param("name") String name);
	
	@Query(value = "SELECT * FROM products  WHERE product_name like %:name%", nativeQuery = true)
	List<CProduct> findProductByNamePageable(@Param("name") String name,Pageable pageable);
	
	@Query(value = "SELECT * FROM products  WHERE product_name like %:name% ORDER BY product_name DESC", nativeQuery = true)
	List<CProduct> findProductByNameDESC(@Param("name") String name,Pageable pageable);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE products SET product_name = :name WHERE product_name IS NULL", nativeQuery = true)
	int updateProductName(@Param("name") String name);
}
