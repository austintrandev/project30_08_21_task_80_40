package com.shoppizza365.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.shoppizza365.model.*;

public interface IProductLineRepository extends JpaRepository<CProductLine, Long> {
	@Query(value = "SELECT * FROM product_lines  WHERE product_line like %:line%", nativeQuery = true)
	List<CProductLine> findProductLineByLine(@Param("line") String line);
	
	@Query(value = "SELECT * FROM product_lines  WHERE product_line like %:line%", nativeQuery = true)
	List<CProductLine> findProductLineByLinePageable(@Param("line") String line,Pageable pageable);
	
	@Query(value = "SELECT * FROM product_lines  WHERE product_line like %:line% ORDER BY product_line DESC", nativeQuery = true)
	List<CProductLine> findProductLineByLineDESC(@Param("line") String line,Pageable pageable);
	
	@Transactional
	@Modifying
	@Query(value = "UPDATE product_lines SET product_line = :line WHERE product_line IS NULL", nativeQuery = true)
	int updateProductLine(@Param("line") String line);
}
